;; this is a comment
;; this is another comment

(def not (lambda (x) (eq nil x)))
(def last (lambda (L)
	    (cond
	      ((null (cdr L)) (car L))
	      (t (last (cdr L))))))
(def append (lambda (x y z)
	      (cond
		((not (null x)) (cons (car x)
				      (append (cdr x) y z)))
		((not (null y)) (cons (car y)
				      (append nil (cdr y) z)))
		(t z))))

(def test (lambda (name a b) ;; function to run a test
	    (let (
		  (l (append nil name
			     (cons (cond
				     ((eq a b) 'OK)
				     (t 'FAIL))))))
	      (write-list l) (eq 'OK (last l)))))

(def run-tests (lambda (l)
		 (cond
		   ((null l) '(0 . 0))
		   (t (let (
			    (r (eval (car l)))
			    (q (run-tests (cdr l)))
			    )
			(cond
			  ((eq t r) (cons (+ 1 (car q)) (cdr q)))
			  (t (cons (car q) (+ 1 (cdr q))))))))))


(def fact (lambda (n)
	    (cond
	      ((eq n 1) 1)
	      (t (* n (fact (- n 1)))))))

(def fib (lambda (x)
	   (cond
	    ((eq x 1) 1)
	    ((eq x 2) 1)
	    (t (+ (fib (- x 1)) (fib (- x 2)))))))

(def map (lambda (f l)
	   (cond
	     ((null l) nil)
	     (t (cons (f (car l)) (map f (cdr l)))))))

(def fct (lambda (x)
	   (lambda (y)
	     (* x y))))

(let
    ((results (run-tests '(
			   (test '(test car:) (car '(1 . 2)) 1)
			   (test '(test cdr:) (cdr '(1 . 2)) 2)
			   (test '(test eq:) T (eq nil nil))
			   (test '(test lambda:)
			    ((lambda (x) (car x)) '(1 2)) 1)
			   (test '(test fact 10:) (fact 10) 3628800)
			   (test '(test fib 20:) (fib 20) 6765)
			   (test '(test fib 10:) (fib 10) 55)
			   (test '(test let and add:)
				 (let ((x 42) (y 43) (z 44)) (+ x y z)) 129)
			   )
			 )))
  (write)
  (write '*** 'results:
	 (car results) 'OK,
	 (cdr results) 'KO,
	 (+ (car results) (cdr results)) 'Total. )
  (write))
