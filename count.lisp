(defun count (l n)
  (cond
   ((null l) n)
   (t (count (cdr l) (+ n 1)))
   ))
(write (count (quote (1 2 3 4 5)) 0))
