/*
** LISP, second try
*/

#define HASH_SIZE 512

enum e_type {
	ETYPE_ATOM,
	ETYPE_CONS,
	ETYPE_INT,
	ETYPE_TRUE,
	ETYPE_FCT,
	ETYPE_BUILTIN,
	ETYPE_ERROR
};

struct ctx;

struct atom {
	char *str;
	int hash;
};

struct elem {
	int id;
	enum e_type type;
	union {
		struct atom atom;
		long long int integer;
		struct elem *cons[2];
		struct elem *(*fct)(struct elem *args, struct ctx *ctx);
	} data;
	int tag;
	int free;
};

struct ctx {
	int collect;
	struct elem *root;
	struct elem *globals[HASH_SIZE];
	struct elem *stack[HASH_SIZE];
};

struct elem *new_elem();
void set_global_ctx(struct ctx *ctx);
void gc_mark_elem(struct elem *e);
struct elem *eval(struct elem *e, struct ctx *ctx);
struct elem *evalList(struct elem *e, struct ctx *ctx);
struct elem *evalAll(struct elem *e, struct ctx *ctx);
struct elem *cons(struct elem *car, struct elem *cdr);
struct elem *car(struct elem *cons);
struct elem *cdr(struct elem *cons);
struct elem *atom(char *name);
struct elem *tru();
struct elem *integer(long long int nbr);
struct elem *builtin(struct elem *(*fct)(struct elem *e, struct ctx *ctx));
struct elem *function(struct elem *p, struct elem *b, struct elem *c);
struct elem *error(char *message, ...);
void free_elem(struct elem *elem);
int sprint_elem(const struct elem *elem);
int fprint_elem(FILE *stream, const struct elem *elem);
int print_elem(const struct elem *elem);

struct elem *parse(char **root);

int get_hash(char *name);
struct atom *new_atom(char *str);
void free_atoms();

struct ctx *new_ctx();
struct elem *ctx_add_global(struct ctx *ctx, char *name, struct elem *data);
struct elem *ctx_push(struct ctx *ctx, char *name, struct elem *data);
void ctx_pop(struct ctx *ctx, char *name);
struct elem *ctx_get(struct ctx *ctx, struct elem *e);
struct elem *ctx_to_list(struct ctx *ctx);
void ctx_mark_elem(struct elem *e, int mark);
void ctx_mark_all(struct ctx *ctx, int mark);
void debug_ctx(struct ctx *ctx, int what);
void free_ctx(struct ctx *ctx);

void init_builtins(struct ctx *ctx);
