(labels
 (
  (rev (lst res)
       (cond
        ((eq nil (null lst)) (rev (cdr lst) (cons (car lst) res)))
        (t res)
        )
       )
  (not (x) (eq x nil))
  (app (l1 l2 l3 res)
       (cond
        ((not (null l1)) (app (cdr l1) l2 l3 (cons (car l1) res)))
        ((not (null l2)) (app nil (cdr l2) l3 (cons (car l2) res)))
        ((not (null l3)) (app nil nil (cdr l3) (cons (car l3) res)))
        (t (rev res ()))
        ))
  (append (l1 l2 l3)
           (app l1 l2 l3 ()))

  )
 (write (append (quote (1 2 3)) (quote (4 5 6)) (quote (7 8 9))))
 )
