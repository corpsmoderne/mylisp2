/*
** LISP, second try
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lisp.h"

#define CHUNK 2048

static int ncount = 0;
static int nchunk = 0;
static struct elem **elems = NULL;
static struct elem *freeLst = NULL;

static struct ctx *global_ctx = NULL;
int mark = 42;

void set_global_ctx(struct ctx *ctx)
{
	global_ctx = ctx;
}

void print_elems()
{
	for(int i=0; i < ncount-1; ++i) {
		printf("[%i/%i/%i] {%i} ", i, ncount, (nchunk*CHUNK)-1,
		       (&elems[i/CHUNK][i%CHUNK])->tag);
		print_elem(&elems[i/CHUNK][i%CHUNK]);
		printf("\n");
	}
	printf("\n");
}

void gc_mark_elem(struct elem *e)
{
	ctx_mark_elem(e, mark);
}

int reclaim_all(int mark, int n)
{
	int reclaimed = 0;
	int to = CHUNK * (ncount / CHUNK);
	for(int i=0; i < to && reclaimed < n; ++i) {
		struct elem *e = &(elems[i/CHUNK][i%CHUNK]);
		//printf("-- %i %i | %i %i \n",
		//i/CHUNK, i%CHUNK, nchunk, nchunk*CHUNK);
		//printf("[%i | %i] check: {%i != %i} ", i, to, e->tag, mark);
		if (e->free == 0 && e->tag > 0 && e->tag < mark) {
			reclaimed++;
			printf("RECLAIM %i", e->id);
			//print_elem(e);
			printf("\n");
			e->type = ETYPE_CONS;
			e->data.cons[1] = freeLst;
			//e->data.cons[0] = NULL;
			e->free = 1;
			freeLst = e;
		}
	}
	return reclaimed;
}

struct elem *new_elem()
{
	struct elem *ret;
	static int id = 0;

	if ((ncount+1) / CHUNK >= nchunk-1) {
		if (freeLst != NULL) {
			ret = freeLst;
			ret->free = 0;
			freeLst = ret->data.cons[1];
			ret->data.cons[1] = NULL;
			return ret;
		}

		if (global_ctx->collect) {
			ctx_mark_all(global_ctx, mark);
			int reclaimed = reclaim_all(mark, 10240);
			printf(">> reclaimed: %i {%i}\n", reclaimed, mark);
			++mark;
			if (reclaimed > 0) {
				return new_elem();
			}
		}

		nchunk++;
		elems = realloc(elems, sizeof(struct elem *)*nchunk);
		elems[nchunk-1] = malloc(sizeof(struct elem)*CHUNK);
	}
	ret = &(elems[ncount/CHUNK][ncount%CHUNK]);
	ret->free = 0;
	ret->id = id++;
	ret->tag = mark;
	ncount++;
	return ret;

        // return malloc(sizeof(struct elem));
}
