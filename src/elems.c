/*
** LISP, second try
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "lisp.h"

struct elem *cons(struct elem *car, struct elem *cdr)
{
	struct elem *elem = new_elem();
	elem->type = ETYPE_CONS;
	elem->data.cons[0] = car;
	elem->data.cons[1] = cdr;
	return elem;
}

struct elem *car(struct elem *cons)
{
	if (cons == NULL || cons->type != ETYPE_CONS) {
		return NULL;
	}
	return cons->data.cons[0];
}

struct elem *cdr(struct elem *cons)
{
	if (cons == NULL || cons->type != ETYPE_CONS) {
		return NULL;
	}
	return cons->data.cons[1];
}

struct elem *atom(char *name)
{
	struct elem *elem = new_elem();
	elem->type = ETYPE_ATOM;
	struct atom *a = new_atom(name);
	elem->data.atom.str = a->str;
	elem->data.atom.hash = a->hash;
	return elem;
}

struct elem *integer(long long int nbr)
{
	struct elem *elem = new_elem();
	elem->type = ETYPE_INT;
	elem->data.integer = nbr;
	return elem;
}

struct elem *builtin(struct elem *(*fct)(struct elem *e, struct ctx *ctx))
{
	struct elem *elem = new_elem();
	elem->type = ETYPE_BUILTIN;
	elem->data.fct = fct;
	return elem;
}

struct elem *function(struct elem *params,
		      struct elem *body,
		      struct elem *closure)
{
	struct elem *elem = cons(params, cons(body, cons(closure, NULL)));
	elem->type = ETYPE_FCT;
	return elem;
}

struct elem *tru()
{
	struct elem *elem = new_elem();
	elem->type = ETYPE_TRUE;
	return elem;
}

struct elem *error(char *message, ...)
{
	va_list ap;
	char buff[256];

	va_start(ap, message);
	vsprintf(buff, message, ap);
	struct elem *elem = atom(buff);
	va_end(ap);
	elem->type = ETYPE_ERROR;
	return elem;
}

void free_elem(struct elem *elem)
{
	if (elem == NULL) {
		return;
	} else if (elem->type == ETYPE_CONS) {
		free_elem(elem->data.cons[0]);
		free_elem(elem->data.cons[1]);
	}
	free(elem);
}

int fprint_elem(FILE *stream, const struct elem *elem)
{
	int len = 0;

	if (elem == NULL) {
		len = fprintf(stream, "NIL");
		return len;
	}

	fprintf(stream, "[%i]", elem->id);

	switch(elem->type) {
	case ETYPE_ERROR:
		len += fprintf(stream,"*** ERROR: %s", elem->data.atom.str);
		break;
	case ETYPE_ATOM:
		len += fprintf(stream,"%s", elem->data.atom.str);
		break;
	case ETYPE_INT:
		len += fprintf(stream,"%lli", elem->data.integer);
		break;
	case ETYPE_TRUE:
		len += fprintf(stream,"T");
		break;
	case ETYPE_BUILTIN:
		len += fprintf(stream,"#<BUILTIN>");
		break;
	case ETYPE_FCT:
		len += fprintf(stream,"#<FUNCTION :LAMBDA ");
		len += fprint_elem(stream, elem->data.cons[0]);
		len += fprintf(stream," ");
		len += fprint_elem(stream, elem->data.cons[1]->data.cons[0]);
		len += fprintf(stream,">");
		break;
	case ETYPE_CONS:
		len += fprintf(stream,"(");
		while(elem) {
			len += fprint_elem(stream, elem->data.cons[0]);
			elem = elem->data.cons[1];
			if (elem) {
				len += fprintf(stream," ");
				if (elem->type != ETYPE_CONS) {
					len += fprintf(stream,". ");
					len += fprint_elem(stream, elem);
					break;
				}
			}
		}
		len += fprintf(stream,")");
		break;
	default:
		break;
	}
	return len;
}

int print_elem(const struct elem *elem)
{
	return fprint_elem(stdout, elem);
}
