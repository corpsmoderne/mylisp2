/*
** LISP, second try
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lisp.h"

static const int atom_chunks = 64;
static int atom_chunk_n = 0;
static int atom_size = 0;
static struct atom **atoms;

int get_hash(char *name)
{
	int h = 0;
	while(*name) {
		h += *name;
		name++;
	}
	return h % HASH_SIZE;
}

struct atom *new_atom(char *str)
{
	int hash = get_hash(str);
	for(int i = 0; i < atom_size; ++i) {
		if (hash == atoms[i]->hash &&
		    strcmp(str, atoms[i]->str) == 0) {
			return atoms[i];
		}
	}
	if (atom_size >= atom_chunk_n * atom_chunks) {
		++atom_chunk_n;
		atoms = realloc(atoms,
				atom_chunk_n * atom_chunks *
				sizeof(struct atom));
	}
	atoms[atom_size] = malloc(sizeof(struct atom));
	atoms[atom_size]->str = strdup(str);
	atoms[atom_size]->hash = hash;
	++atom_size;
	return atoms[atom_size-1];
}

void debug_atom()
{
	for(int i=0; i < atom_size; ++i) {
		printf("%s -> %i\n", atoms[i]->str, atoms[i]->hash); 
	}
}

void free_atoms()
{
	if (atoms) {
		free(atoms);
		atoms = NULL;
	}
}
