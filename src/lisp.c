/*
** LISP, second try
*/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "lisp.h"

struct elem *assoc(struct elem *params, struct elem *args, struct ctx *ctx)
{
	while(params) {
		if (args == NULL) {
			return error("assoc: not enough params");
		}
		ctx_push(ctx, car(params)->data.atom.str, eval(car(args), ctx));
		params = cdr(params);
		args = cdr(args);
	}
	if (args) {
		return error("assoc: too many params");
	}
	return NULL;
}

void pop_assoc(struct elem *params, struct ctx *ctx)
{
	while(params) {
		ctx_pop(ctx, car(params)->data.atom.str);
		params = cdr(params);
	}
}

/*
static void add_closure_vals(struct elem *fct, struct ctx *ctx)
{
	struct elem *closure = fct->data.cons[1]->data.cons[1]->data.cons[0];
	while(closure) {
		ctx->stack = cons(closure->data.cons[0],
				  ctx->stack);
		closure = closure->data.cons[1];
	}
}
*/

struct elem *apply(struct elem *fn, struct elem *x, struct ctx *ctx)
{
	struct elem *fct = eval(fn, ctx);
	if (fct) {
		if (fct->type == ETYPE_BUILTIN) {
			return fct->data.fct(x, ctx);
		}
		if (fct->type == ETYPE_FCT) {
			//add_closure_vals(fct, ctx);
			struct elem *ret = assoc(fct->data.cons[0], x, ctx);
			if (!ret) {
				ret = evalList(fct->data.cons[1]->data.cons[0],
					       ctx);
			}
			pop_assoc(fct->data.cons[0], ctx);
			return ret;
		}
	}
	if (fn && fn->type == ETYPE_ATOM) {
		return error("undefined function '%s'", fn->data.atom.str);
	}
	if (fn && fn->type == ETYPE_INT) {
		return error("'%i' is not function name, try an atom instead",
			     fn->data.integer);
	}
	print_elem(fn);
	return error("undefined function (wtf?)");
}

struct elem *eval(struct elem *e, struct ctx *ctx)
{
	struct elem *ret = e;
	if (e == NULL) {
		return NULL;
	}
	switch(e->type) {
	case ETYPE_ATOM:
		ret = ctx_get(ctx, e);
		break;
	case ETYPE_CONS:
		ret = apply(car(e), cdr(e), ctx);
		break;
	default:
		break;
	}
	return ret;
}

struct elem *evalList(struct elem *e, struct ctx *ctx)
{
	struct elem *last = NULL;
	while(e) {
		last = eval(car(e), ctx);
		e = cdr(e);
	}
	return last;
}

struct elem *evalAll(struct elem *e, struct ctx *ctx)
{
	if (e == NULL) {
		return NULL;
	}
	struct elem *a = eval(car(e), ctx);
	if (a && a->type == ETYPE_ERROR) {
		return a;
	}
	struct elem *next = evalAll(cdr(e), ctx);
	if (next && next->type == ETYPE_ERROR) {
		return next;
	}
	return cons(a, next);
}
