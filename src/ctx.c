/*
** LISP, second try
*/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "lisp.h"

struct ctx *new_ctx()
{
	struct ctx *ctx = malloc(sizeof(struct ctx));
	memset(ctx, 0, sizeof(struct ctx));
	return ctx;
}

struct elem *ctx_add_global(struct ctx *ctx, char *name, struct elem *data)
{
	int hash = get_hash(name);
	struct elem *c = cons(cons(atom(name), data),
			      ctx->globals[hash]);

	ctx->globals[hash] = c;
	return car(c);
}

struct elem *ctx_push(struct ctx *ctx, char *name, struct elem *data)
{
	int hash = get_hash(name);

	struct elem *c = cons(cons(atom(name), data),
			      ctx->stack[hash]);
	ctx->stack[hash] = c;
	return car(c);
}

/*
static struct elem *ctx_pop_(struct elem *e, char *name)
{
	if (e == NULL) {
		return NULL;
	}
	if (car(car(e))->data.atom == name) {
		return cdr(e);
	} else {
		return ctx_pop_(cdr(e), name);
	}
}
*/
void ctx_pop(struct ctx *ctx, char *name)
{
	int hash = get_hash(name);
	//struct elem *c = ctx->stack[hash];

	ctx->stack[hash] = cdr(ctx->stack[hash]); //ctx_pop_(c, name);
}

static struct elem *find(struct elem *lst, char *name)
{
	while (lst) {
		if (name == car(car(lst))->data.atom.str) {
			return cdr(car(lst));
		}
		lst = cdr(lst);
	}
	return (struct elem *)(-1);
}
struct elem *ctx_get(struct ctx *ctx, struct elem *e)
{
	int hash = e->data.atom.hash;

	struct elem *ret = find(ctx->globals[hash], e->data.atom.str);
	if (ret == (struct elem *)(-1)) {
		ret = find(ctx->stack[hash], e->data.atom.str);
	}
	if (ret == (struct elem *)(-1)) {
		ret = error("variable '%s' has no value", e->data.atom.str);
	}
	return ret;
}

struct elem *ctx_to_list(struct ctx *ctx)
{
	struct elem *ret = NULL;

	for(int i=0; i < HASH_SIZE; ++i) {
		struct elem *e = ctx->globals[i];
		while(e) {
			ret = cons(car(car(e)), ret);
			e = cdr(e);
		}
	}

	return ret;
}

void ctx_mark_elem(struct elem *e, int mark)
{
	if (e == NULL) {
		return;
	}
	if (e->tag == mark || e->free == 1) {
		return;
	}
	e->tag = mark;
	//printf("mark %i [%i]", mark, e->id);
	//print_elem(e);
	//printf("\n");
	if (e->type == ETYPE_CONS || e->type == ETYPE_FCT) {
		ctx_mark_elem(e->data.cons[0], mark);
		ctx_mark_elem(e->data.cons[1], mark);
	}
}

void ctx_mark_all(struct ctx *ctx, int mark)
{
	ctx_mark_elem(ctx->root, mark);
	for(int i=0; i < HASH_SIZE; ++i) {
		ctx_mark_elem(ctx->globals[i], mark);
		ctx_mark_elem(ctx->stack[i], mark);
	}
}

static void debug_hash(struct elem **elems) {
	for(int i=0; i < HASH_SIZE; ++i) {
		if (elems[i] != NULL) {
			printf("   [%.3i|%i] ", i, elems[i]->tag);
			print_elem(elems[i]);
			printf("\n");
		}
	}
}

void debug_ctx(struct ctx *ctx, int what)
{
	if (what & 1) {
		printf("** GLOBALS:\n");
		debug_hash(ctx->globals);
		printf("\n");
	}
	if (what & 2) {
		printf("** STACK:\n   ");
		debug_hash(ctx->stack);
		printf("\n");
	}
}

void free_ctx(struct ctx *ctx)
{
	/*
	for(int i=0; i < HASH_SIZE; ++i) {
		free_elem(ctx->globals[i]);
	}
	free_elem(ctx->stack);
	*/
}
