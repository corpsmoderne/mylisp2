/*
** LISP, second try
*/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "lisp.h"

static int is_sep(char c)
{
	return (c == ' ' || c == '\t' || c == '\n');
}

static void eat_sep(char **str)
{
	while(*str && **str && is_sep(**str)) {
		++(*str);
	}
}

static void eat_comment(char **str)
{
	while(*str && **str != '\n') {
		++(*str);
	}
}

static struct elem *str_to_elem(char *str)
{
	char *endptr;
	long int val = strtol(str, &endptr, 10);
	struct elem *ret;

	if (endptr-str-strlen(str) == 0) {
		ret = integer(val);
	} else if (strcmp(str, "nil") == 0) {
		ret = NULL;
	} else {
		ret = atom(str);
	}
	return ret;
}

static struct elem *parse_atom(char **str)
{
	int i = 0;
	struct elem *ret;

	eat_sep(str);
	while(!is_sep((*str)[i])
	      && (*str)[i] && (*str)[i] != ')' && (*str)[i] != '(') {
		++i;
	}
	if (i == 0) {
		return NULL;
	}
	char *a = strndup(*str, i);
	ret = str_to_elem(a);
	free(a);
	*str += i;
	return ret;
}

static struct elem *parse_list(char **str)
{
	if (strlen(*str) == 0) {
		return error("Parsing: ')' expected, got: '%s'", *str);
	}
	eat_sep(str);
	if (**str == ')') {
		++(*str);
		return NULL;
	}
	if (**str == '.') {
		(*str)++;
		struct elem *cdr = parse(str);
		eat_sep(str);
		if (**str != ')') {
			++(*str);
			return error("Parsing: ')' expected, "
				     "got: '%s'", *str);
		}
		++(*str);
		return cdr;
	} else {
		struct elem *car = parse(str);
		if (car && car->type == ETYPE_ERROR) {
			return car;
		}
		struct elem *ret = parse_list(str);
		if (ret && ret->type == ETYPE_ERROR) {
			return ret;
		}
		return cons(car, ret);
	}
}

struct elem *parse(char **str)
{
	struct elem *ret = NULL;

	eat_sep(str);
	switch(**str) {
	case ';':
		eat_comment(str);
		break;
	case ')':
		++(*str);
		ret = error("Parsing: got unmatched ')'");
		break;
	case '\'':
		++(*str);
		ret = parse(str);
		if (!ret || ret->type != ETYPE_ERROR) {
			ret = cons(atom("quote"), cons(ret, NULL));
		}
		break;
	case '(':
		++(*str);
		ret = parse_list(str);
		break;
	default:
		ret = parse_atom(str);
		break;
	}
	return ret;
}
