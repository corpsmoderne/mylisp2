/*
** LISP, second try
*/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "lisp.h"

#define EVAL_OR_RETURN_ERROR(ARGS,CTX) {			\
		ARGS = evalAll(ARGS, CTX);			\
		if (ARGS && ARGS->type == ETYPE_ERROR) {	\
			return ARGS;				\
		}						\
	}

static struct elem *fct_cons(struct elem *args, struct ctx *ctx)
{
	EVAL_OR_RETURN_ERROR(args,ctx);
	struct elem *arg1 = car(args);
	struct elem *arg2 = car(cdr(args));
	return cons(arg1, arg2);
}

static struct elem *fct_car(struct elem *args, struct ctx *ctx)
{
	EVAL_OR_RETURN_ERROR(args,ctx);
	return car(car(args));
}

static struct elem *fct_cdr(struct elem *args, struct ctx *ctx)
{
	EVAL_OR_RETURN_ERROR(args,ctx);
	return cdr(car(args));
}

static struct elem *fct_quote(struct elem *args, struct ctx *ctx)
{
	return car(args);
}

static struct elem *fct_eq(struct elem *args, struct ctx *ctx)
{
	EVAL_OR_RETURN_ERROR(args, ctx);
	struct elem *arg1 = car(args);
	struct elem *arg2 = car(cdr(args));

	if (arg1 == NULL && arg2 == NULL) {
		return tru();
	}
	if (arg1 == NULL || arg2 == NULL || arg1->type != arg2->type) {
		return NULL;
	}
	if (arg1->type == ETYPE_TRUE) {
		return tru();
	}
	if (arg1->type == ETYPE_ATOM &&
	    arg1->data.atom.str == arg2->data.atom.str) {
		return tru();
	}
	if (arg1->type == ETYPE_INT &&
	    arg1->data.integer == arg2->data.integer) {
		return tru();
	}
	return NULL;
}

static struct elem *fct_null(struct elem *args, struct ctx *ctx)
{
	EVAL_OR_RETURN_ERROR(args, ctx);
	struct elem *arg1 = car(args);
	if (arg1 == NULL) {
		return tru();
	} else {
		return NULL;
	}
}

static struct elem *fct_lambda(struct elem *args, struct ctx *ctx)
{
	struct elem *params = car(args);
	struct elem *body = cdr(args);

	struct elem *fct = function(params, body, NULL); //ctx->stack);
	return fct;
}

static struct elem *fct_cond(struct elem *args, struct ctx *ctx)
{
	while(args) {
		struct elem *cond = eval(car(car(args)), ctx);
		if (cond && cond->type == ETYPE_ERROR) {
			return cond;
		}
		if (cond && cond->type == ETYPE_TRUE) {
			return evalList(cdr(car(args)), ctx);
		}
		args = cdr(args);
	}
	return NULL;
}

static struct elem *fct_def(struct elem *args, struct ctx *ctx)
{
	struct elem *name = car(args);
	if (name->type != ETYPE_ATOM) {
		return error("first arg must be an atom");
	}
	struct elem *val = eval(car(cdr(args)), ctx);
	if (val && val->type == ETYPE_ERROR) {
		return val;
	}
	ctx_add_global(ctx, name->data.atom.str, val);
	return name;
}

static struct elem *fct_let(struct elem *args, struct ctx *ctx)
{
	struct elem *defs = car(args);
	struct elem *body = cdr(args);

	while(defs) {
		struct elem *val = eval(car(cdr(car(defs))), ctx);
		if (val && val->type == ETYPE_ERROR) {
			return val;
		}
		ctx_push(ctx, car(car(defs))->data.atom.str, val);
		defs = cdr(defs);
	}
	struct elem *ret = evalList(body, ctx);

	defs = car(args);
	while(defs) {
		ctx_pop(ctx, car(car(defs))->data.atom.str);
		defs = cdr(defs);
	}
	
	return ret;
}

static struct elem *fct_add(struct elem *args, struct ctx *ctx)
{
	EVAL_OR_RETURN_ERROR(args, ctx);
	long long int i = 0;
	while(args) {
		struct elem *nbr = car(args);
		if (nbr) {
			i += nbr->data.integer;
		}
		args = cdr(args);
	}
	return integer(i);
}

static struct elem *fct_sub(struct elem *args, struct ctx *ctx)
{
	EVAL_OR_RETURN_ERROR(args, ctx);
	long long int i = 0;
	struct elem *nbr = car(args);
	if (nbr) {
		i = nbr->data.integer;
	}
	args = cdr(args);
	while(args) {
		nbr = eval(car(args), ctx);
		if (nbr) {
			i -= nbr->data.integer;
		}
		args = cdr(args);
	}
	return integer(i);
}

static struct elem *fct_mul(struct elem *args, struct ctx *ctx)
{
	EVAL_OR_RETURN_ERROR(args, ctx);
	long long int i = 1;
	while(args) {
		struct elem *nbr = car(args);
		if (nbr) {
			i *= nbr->data.integer;
		}
		args = cdr(args);
	}
	return integer(i);
}

static struct elem *fct_div(struct elem *args, struct ctx *ctx)
{
	EVAL_OR_RETURN_ERROR(args, ctx);
	long long int i = 0;
	struct elem *nbr = car(args);
	if (nbr) {
		i = nbr->data.integer;
	}
	struct elem *nbr2 = car(cdr(args));
	if (nbr2) {
		i /= nbr2->data.integer;
	}
	return integer(i);
}

static struct elem *fct_modulo(struct elem *args, struct ctx *ctx)
{
	EVAL_OR_RETURN_ERROR(args, ctx);
	long long int i = 0;
	struct elem *nbr = car(args);
	if (nbr) {
		i = nbr->data.integer;
	}
	struct elem *nbr2 = car(cdr(args));
	if (nbr2) {
		i = i % nbr2->data.integer;
	}
	return integer(i);
}

static struct elem *fct_inf(struct elem *args, struct ctx *ctx)
{
	EVAL_OR_RETURN_ERROR(args, ctx);
	struct elem *arg1 = car(args);
	struct elem *arg2 = car(cdr(args));
	
	if (arg1 == NULL || arg2 == NULL) {
		return NULL;
	}
	if (arg1->type == ETYPE_INT && arg2->type == ETYPE_INT &&
	    arg1->data.integer < arg2->data.integer) {
		return tru();
	}
	return NULL;
}

static struct elem *fct_eval(struct elem *args, struct ctx *ctx)
{
	struct elem *ret = eval(car(args), ctx);
	return eval(ret, ctx);
}

static struct elem *fct_write(struct elem *args, struct ctx *ctx)
{
	EVAL_OR_RETURN_ERROR(args, ctx);
	while(args) {
		print_elem(car(args));
		args = cdr(args);
		if (args) {
			printf(" ");
		}
	}
	printf("\n");
	return args;
}

static struct elem *fct_write_list(struct elem *args, struct ctx *ctx)
{
	EVAL_OR_RETURN_ERROR(args, ctx);
	args = car(args);
	while(args) {
		print_elem(car(args));
		args = cdr(args);
		if (args) {
			printf(" ");
		}
	}
	printf("\n");
	return args;
}

static struct elem *fct_debug(struct elem *args, struct ctx *ctx)
{
	int what = 0;
	while(args) {
		if (car(args)) {
			if (strcmp(car(args)->data.atom.str, "globals") == 0) {
				what |= 1;
			}
			if (strcmp(car(args)->data.atom.str, "stack") == 0) {
				what |= 2;
			}
		}
		args = cdr(args);
	}
	debug_ctx(ctx, what);
	return NULL;
}

void init_builtins(struct ctx *ctx)
{
	ctx_add_global(ctx, "cons",   builtin(fct_cons));
	ctx_add_global(ctx, "car",    builtin(fct_car));
	ctx_add_global(ctx, "cdr",    builtin(fct_cdr));
	ctx_add_global(ctx, "quote",  builtin(fct_quote));
	ctx_add_global(ctx, "eq",     builtin(fct_eq));
	ctx_add_global(ctx, "null",   builtin(fct_null));
	ctx_add_global(ctx, "lambda", builtin(fct_lambda));
	ctx_add_global(ctx, "\\",     builtin(fct_lambda));
	ctx_add_global(ctx, "cond",   builtin(fct_cond));

	ctx_add_global(ctx, "def", builtin(fct_def));
	ctx_add_global(ctx, "let", builtin(fct_let));

	ctx_add_global(ctx, "+", builtin(fct_add));
	ctx_add_global(ctx, "-", builtin(fct_sub));
	ctx_add_global(ctx, "*", builtin(fct_mul));
	ctx_add_global(ctx, "/", builtin(fct_div));
	ctx_add_global(ctx, "%", builtin(fct_modulo));
	ctx_add_global(ctx, "<", builtin(fct_inf));

	ctx_add_global(ctx, "eval",       builtin(fct_eval));
	ctx_add_global(ctx, "write",      builtin(fct_write));
	ctx_add_global(ctx, "write-list", builtin(fct_write_list));
	ctx_add_global(ctx, "debug",      builtin(fct_debug));

	ctx_add_global(ctx, "t",   tru());
	ctx_add_global(ctx, "T",   tru());
	ctx_add_global(ctx, "nil", NULL);
	ctx_add_global(ctx, "NIL", NULL);
}
