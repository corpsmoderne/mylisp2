/*
** LISP, second try
*/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "lisp.h"

static const int buff_size = 1024;

char *read_file(char *filename)
{
	char *str = NULL;
	char buff[buff_size];
	int fd = open(filename, O_RDONLY);
	int s = buff_size;
	int total_size = 0;

	while (s == buff_size) {
		s = read(fd, buff, buff_size);
		str = realloc(str, total_size+s);
		memcpy(str+total_size, buff, s);
		total_size += s;
	}
	if (str[total_size-1] == '\n') {
		str[total_size-1] = '\0';
	} else {
		str = realloc(str, total_size+1);
		str[total_size] = '\0';
	}
	close(fd);
	return str;
}

static struct ctx *global_ctx;
char *character_name_generator(const char *text, int state)
{
	static int len;
	static struct elem *lst;

	if (!state) {
		len = strlen(text);
		lst = ctx_to_list(global_ctx);
	}
	while(lst) {
		char *atom = car(lst)->data.atom.str;
		if (lst) {
			lst = cdr(lst);
		}
		if (strncmp(atom, text, len) == 0) {
			return strdup(atom);
		}
	}

	return NULL;
}

char **character_name_completion(const char *text, int start, int end)
{
	rl_attempted_completion_over = 1;
	return rl_completion_matches(text, character_name_generator);
}

struct elem *evalStr(char **s, struct ctx *ctx)
{
	ctx->collect = 0;
	struct elem *root = parse(s);
	gc_mark_elem(root);
	ctx->root = root;
	ctx->collect = 1;
	return eval(root, ctx);
}

void repl(struct ctx *ctx, char *p) {
	int cmd_nbr = 0;

	rl_attempted_completion_function = character_name_completion;

	while(1) {
		char prompt[64];
		if (p == NULL) {
			sprintf(prompt, "MyLisp [%i]> ", ++cmd_nbr);
		} else {
			strcpy(prompt, p);
		}
		char *s = readline(prompt);
		if (s == NULL) {
			printf("\nquit\n");
			break;
		}
		if (strlen(s)) {
			add_history(s);
		}
		while(s && *s) {
			struct elem *ret = evalStr(&s, ctx);
			print_elem(ret);
			printf("\n");
		}
	}
}

int main(int ac, char **av)
{
	struct ctx *ctx = new_ctx();
	global_ctx = ctx;
	set_global_ctx(ctx);
	init_builtins(ctx);

	if (ac > 1) {
		char *buff = read_file(av[1]);
		char *str = buff;
		while(str && *str) {
			struct elem *ret = evalStr(&str, ctx);
			if (ret && ret->type == ETYPE_ERROR) {
				print_elem(ret);
				printf("\n");
			}
		}
		free(buff);
	}
	if (ac == 1 || strcmp(av[ac-1], "-i") == 0) {
		repl(ctx, NULL);
	}

	free_ctx(ctx);
	free_atoms();
}
