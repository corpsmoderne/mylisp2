(defun app (x y z)
  (write-line "")
  (write x) (write y) (write z) (write " : ")
  (cond
   ((not (null x)) (write (car x)) (cons (car x) (app (cdr x) y z)))
   ((not (null y)) (write (car y)) (cons (car y) (app nil (cdr y) z)))
   (t (write-line "") z)))

(write (app '(1 2 3) '(4 5 6) '(7 8 9)))
(write-line "")

(defun isin (x y)
  (cond
   ((null y) nil)
   ((eq x (car y)) t)
   (t (isin x (cdr y)))))

(write "toto is in list (yes): ")
(write (isin 'toto '(titi tutu toto tata)))
(write-line "")
(write "foobar is in list(no): ")
(write (isin 'foobar '(titi tutu toto tata)))
(write-line "")
