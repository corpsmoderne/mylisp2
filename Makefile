
NAME = mylisp2

SRC =   src/atoms.c src/elems.c src/parse.c    src/main.c \
	src/lisp.c  src/ctx.c   src/builtins.c src/gc.c

OBJ = $(SRC:.c=.o)

CFLAGS = -Wall --pedantic -g3 -I./include

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(OBJ) -o $(NAME) -lreadline

clean:
	$(RM) $(OBJ) *~

fclean: clean
	$(RM) $(NAME)

re: fclean all
